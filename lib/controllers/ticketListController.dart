import 'package:MJN/Network/MjnAPI.dart';
import 'package:MJN/models/ticketListVO.dart';
import 'package:get/state_manager.dart';

class TicketListController extends GetxController{

  TicketListVo ticketListVo;
  var isLoading = true.obs;

  void fetchTicketList(String token,String uid,String tenantID) async {
    try {
      isLoading(true);
      print(token);
      print(uid);
      print(tenantID);

      var res = await MjnAPI.fetchTicketList(token,uid,tenantID);


      if (res != null) {
        ticketListVo = res;
        print(ticketListVo.status);
        print(ticketListVo.details[0].status);
      }
    } finally {
      isLoading(false);
    }
  }

}